CREATE DATABASE  IF NOT EXISTS `entidad` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `entidad`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: entidad
-- ------------------------------------------------------
-- Server version	5.5.5-10.4.11-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_cuentas`
--

DROP TABLE IF EXISTS `tbl_cuentas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_cuentas` (
  `Id_Cuenta` int(11) NOT NULL AUTO_INCREMENT,
  `Numero_Cuenta` varchar(30) NOT NULL,
  `Deposito` double DEFAULT 0,
  `Estado` varchar(10) NOT NULL DEFAULT 'Activo',
  PRIMARY KEY (`Id_Cuenta`),
  UNIQUE KEY `Numero_Cuenta_UNIQUE` (`Numero_Cuenta`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_cuentas`
--

LOCK TABLES `tbl_cuentas` WRITE;
/*!40000 ALTER TABLE `tbl_cuentas` DISABLE KEYS */;
INSERT INTO `tbl_cuentas` VALUES (1,'667788',0,'Activo'),(2,'667789',200,'Activo'),(3,'334455',50,'Activo'),(4,'991011',0,'Inactivo'),(5,'991012',100,'Activo'),(6,'99109',20,'Activo'),(7,'889977',70,'Activo'),(8,'556',0,'Activo'),(11,'5557',0,'Activo'),(13,'55578',0,'Activo'),(14,'8877',0,'Activo');
/*!40000 ALTER TABLE `tbl_cuentas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_movimientos`
--

DROP TABLE IF EXISTS `tbl_movimientos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_movimientos` (
  `Id_Movimiento` int(11) NOT NULL AUTO_INCREMENT,
  `Id_CuentaOrigen` int(11) NOT NULL,
  `Id_CuentaDestino` int(11) NOT NULL,
  `Id_transferencia` int(11) NOT NULL,
  PRIMARY KEY (`Id_Movimiento`),
  KEY `FK_IdCuentaOrigen_tblmovimientos_idx` (`Id_CuentaOrigen`),
  KEY `FK_IdCuentaDestino_tblmovimientos_idx` (`Id_CuentaDestino`),
  KEY `FK_IdTransferencia_tblmovimientos_idx` (`Id_transferencia`),
  CONSTRAINT `FK_IdCuentaDestino_tblmovimientos` FOREIGN KEY (`Id_CuentaDestino`) REFERENCES `tbl_cuentas` (`Id_Cuenta`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_IdCuentaOrigen_tblmovimientos` FOREIGN KEY (`Id_CuentaOrigen`) REFERENCES `tbl_cuentas` (`Id_Cuenta`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_IdTransferencia_tblmovimientos` FOREIGN KEY (`Id_transferencia`) REFERENCES `tbl_transferencias` (`Id_Transferencia`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_movimientos`
--

LOCK TABLES `tbl_movimientos` WRITE;
/*!40000 ALTER TABLE `tbl_movimientos` DISABLE KEYS */;
INSERT INTO `tbl_movimientos` VALUES (21,1,2,41),(22,2,1,40),(23,1,4,54),(24,1,5,55);
/*!40000 ALTER TABLE `tbl_movimientos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_personas`
--

DROP TABLE IF EXISTS `tbl_personas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_personas` (
  `Id_Persona` int(11) NOT NULL AUTO_INCREMENT,
  `Nombres` varchar(25) NOT NULL,
  `Apellidos` varchar(25) NOT NULL,
  `Correo` varchar(25) NOT NULL,
  PRIMARY KEY (`Id_Persona`),
  UNIQUE KEY `Correo_UNIQUE` (`Correo`)
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_personas`
--

LOCK TABLES `tbl_personas` WRITE;
/*!40000 ALTER TABLE `tbl_personas` DISABLE KEYS */;
INSERT INTO `tbl_personas` VALUES (1,'Jhon','Garcia','JhonGarcia@hmt.com'),(2,'Carlos','Garcia','CarlosGarcia@hmt.com'),(3,'Andrea','Villada','AndreVill@hmt.com'),(4,'Nataly','Lopez','NatalyLopez@hmt.com'),(78,'rik','tn','rikk');
/*!40000 ALTER TABLE `tbl_personas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_transferencias`
--

DROP TABLE IF EXISTS `tbl_transferencias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_transferencias` (
  `Id_Transferencia` int(11) NOT NULL AUTO_INCREMENT,
  `Cantidad_Transferencia` double NOT NULL,
  `Fecha_Transferencia` datetime NOT NULL,
  `Estado` varchar(15) NOT NULL DEFAULT 'Activa',
  PRIMARY KEY (`Id_Transferencia`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_transferencias`
--

LOCK TABLES `tbl_transferencias` WRITE;
/*!40000 ALTER TABLE `tbl_transferencias` DISABLE KEYS */;
INSERT INTO `tbl_transferencias` VALUES (1,10,'2020-03-29 15:30:00','Activa'),(2,100,'2020-03-28 12:00:00','Activa'),(40,44,'2020-03-31 00:00:00','Activa'),(41,32,'2020-03-31 23:06:00','Activa'),(42,76,'2020-03-28 16:11:00','Activa'),(54,18,'2020-03-28 16:12:00','Activa'),(55,76,'2020-03-26 16:41:00','Activa');
/*!40000 ALTER TABLE `tbl_transferencias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_usuarios`
--

DROP TABLE IF EXISTS `tbl_usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_usuarios` (
  `Id_Usuario` int(11) NOT NULL AUTO_INCREMENT,
  `Clave` varchar(30) NOT NULL,
  `Contrasenia` varchar(100) NOT NULL,
  `Id_Persona` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id_Usuario`),
  UNIQUE KEY `Clave_UNIQUE` (`Clave`),
  KEY `FK_IdPersona_idx` (`Id_Persona`),
  CONSTRAINT `FK_IdPersona_tblusuarios` FOREIGN KEY (`Id_Persona`) REFERENCES `tbl_personas` (`Id_Persona`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_usuarios`
--

LOCK TABLES `tbl_usuarios` WRITE;
/*!40000 ALTER TABLE `tbl_usuarios` DISABLE KEYS */;
INSERT INTO `tbl_usuarios` VALUES (1,'123456','123',1),(2,'123455','123',2),(4,'123453','123',4),(18,'1144','123',78),(19,'1155','123',NULL);
/*!40000 ALTER TABLE `tbl_usuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_usuarioxcuenta`
--

DROP TABLE IF EXISTS `tbl_usuarioxcuenta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_usuarioxcuenta` (
  `Id_UsuarioxCuenta` int(11) NOT NULL AUTO_INCREMENT,
  `Id_Usuario` int(11) NOT NULL,
  `Id_Cuenta` int(11) NOT NULL,
  PRIMARY KEY (`Id_UsuarioxCuenta`),
  KEY `FK_IdUsuario_tblusuarioxcuenta_idx` (`Id_Usuario`),
  KEY `FK_IdCuenta_tblusuarioxcuenta_idx` (`Id_Cuenta`),
  CONSTRAINT `FK_IdCuenta_tblusuarioxcuenta` FOREIGN KEY (`Id_Cuenta`) REFERENCES `tbl_cuentas` (`Id_Cuenta`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_IdUsuario_tbl_usuarioxcuenta` FOREIGN KEY (`Id_Usuario`) REFERENCES `tbl_usuarios` (`Id_Usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_usuarioxcuenta`
--

LOCK TABLES `tbl_usuarioxcuenta` WRITE;
/*!40000 ALTER TABLE `tbl_usuarioxcuenta` DISABLE KEYS */;
INSERT INTO `tbl_usuarioxcuenta` VALUES (15,18,1),(16,2,2),(17,4,4),(18,2,5);
/*!40000 ALTER TABLE `tbl_usuarioxcuenta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'entidad'
--

--
-- Dumping routines for database 'entidad'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-03-23 16:30:56
