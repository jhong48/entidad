package Logica;

import java.util.List;

import Datos.Cuenta;
import Datos.Transferencia;

public class Movimiento {
    public String Registrar_Movimiento(String strCuentaOrigen, String strCuentaDestino, int intCantidad, String strFecha_Transferencia, String strHora_Transferencia){
        Datos.Movimiento objMovimiento = new Datos.Movimiento();
        Cuenta objCuentaOrigen = new Cuenta();
        objCuentaOrigen.setNumero_Cuenta(strCuentaOrigen);
        Cuenta objCuentaDestino = new Cuenta();
        Transferencia objTransferencia = new Transferencia();
        objCuentaDestino.setNumero_Cuenta(strCuentaDestino);
        objMovimiento.setObjCuentaOrigen(objCuentaOrigen);
        objMovimiento.setObjCuentaDestino(objCuentaDestino);
        objTransferencia.setCantidad(intCantidad);
        objTransferencia.setFecha_Transferencia(strFecha_Transferencia);
        objTransferencia.setHora_Transferencia(strHora_Transferencia);
        objMovimiento.setObjTransferencia(objTransferencia);
        return objMovimiento.Registrar_Movimiento(objMovimiento);
    }

    public List<Datos.Movimiento> Listar_Movimientos(String strId_Usuario){
        Datos.Movimiento objMovimiento = new Datos.Movimiento();
        return objMovimiento.Listar_Movimientos(strId_Usuario);
    }

    public String Cancelar_Transaccion(String strId_Transferencia, String strEstado){
        Datos.Movimiento objMovimiento = new Datos.Movimiento();
        Transferencia objTransferencia = new Transferencia();
        objTransferencia.setId_Transferencia(Integer.parseInt(strId_Transferencia));
        objTransferencia.setEstado(strEstado);
        objMovimiento.setObjTransferencia(objTransferencia);
        return objMovimiento.Cancelar_Transaccion(objMovimiento);
    }

}
