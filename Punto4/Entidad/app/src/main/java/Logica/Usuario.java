package Logica;

import android.content.Context;

import java.util.List;

public class Usuario {
    public List<Datos.Usuario> Iniciar_Sesion(String strClave, String strContrasenia){
        Datos.Usuario objUsuario = new Datos.Usuario();
        objUsuario.setClave(strClave);
        objUsuario.setContrasenia(strContrasenia);
        return objUsuario.Iniciar_Sesion(objUsuario);
    }

    public boolean Registrar_VariableSession(int intId_Usuario, Context context){
        Datos.Usuario objUsuario = new Datos.Usuario();
        objUsuario.setId_Usuario(intId_Usuario);
        return objUsuario.Registrar_VarialbleSesion(context, objUsuario);

    }

    public String Obtener_VariableSession(Context context){
        Datos.Usuario objUsuario = new Datos.Usuario();
        return objUsuario.Obtener_VariableSesion(context);
    }

    public String Registrar_Usuario(String strClave, String strContrasenia){
        Datos.Usuario objUsuario = new Datos.Usuario();
        objUsuario.setClave(strClave);
        objUsuario.setContrasenia(strContrasenia);
        return objUsuario.Registrar_Usuario(objUsuario);
    }

    public String Registrar_Persona(String strNombres, String strApellidos, String strCorreo, int intId_Usuario){
        Datos.Usuario objUsuario = new Datos.Usuario();
        objUsuario.setNombres(strNombres);
        objUsuario.setApellidos(strApellidos);
        objUsuario.setCorreo(strCorreo);
        objUsuario.setId_Usuario(intId_Usuario);
        return objUsuario.Registrar_Persona(objUsuario);
    }

    public String Verificar_UsuarioPersona(int intId_Usuario){
        Datos.Usuario objUsuario = new Datos.Usuario();
        objUsuario.setId_Usuario(intId_Usuario);
        return objUsuario.Verificar_CuentaPersona(objUsuario);
    }

}
