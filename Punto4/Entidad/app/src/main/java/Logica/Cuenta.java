package Logica;

public class Cuenta {

    public String Registrar_Cuenta(String strNumero_Cuenta, int intId_Usuario){
        Datos.Cuenta objCuenta = new Datos.Cuenta();
        Datos.Usuario objUsusario = new Datos.Usuario();
        objUsusario.setId_Usuario(intId_Usuario);
        objCuenta.setNumero_Cuenta(strNumero_Cuenta);
        objCuenta.setObjUsuario(objUsusario);
        return objCuenta.Registrar_Cuenta(objCuenta);
    }

    public String Verificar_Cuenta(String strNumero_Cuenta){
        Datos.Cuenta objCuenta = new Datos.Cuenta();
        objCuenta.setNumero_Cuenta(strNumero_Cuenta);
        return objCuenta.Verificar_Cuenta(objCuenta);
    }



}
