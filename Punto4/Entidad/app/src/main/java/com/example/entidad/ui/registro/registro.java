package com.example.entidad.ui.registro;


import java.util.Calendar;
import java.util.GregorianCalendar;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TimePicker;

import Logica.Cuenta;
import Logica.Movimiento;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import com.example.entidad.R;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;


import android.widget.Button;

import android.widget.EditText;
import android.widget.Toast;


public class registro extends Fragment implements View.OnClickListener {

    private HomeViewModel homeViewModel;
    Button btnDatePicker, btnTimePicker, btnRegistro;
    EditText txtFecha, txtTiempo, edtCuentaOrigen, edtCuentaDestino, edtCantidad;
    protected String strFecha="", strHora ="", strVerificarCuenta;
    private int mYear, mMonth, mDay, mHour, mMinute;
    protected Movimiento objMovimiento;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_registro, container, false);
        btnDatePicker=(Button)root.findViewById(R.id.btn_date);
        btnTimePicker=(Button)root.findViewById(R.id.btn_time);
        txtFecha =(EditText)root.findViewById(R.id.in_date);
        txtTiempo=(EditText)root.findViewById(R.id.in_time);
        edtCuentaOrigen = (EditText)root.findViewById(R.id.edt_cuentaorigen_ATVRegistroT);
        edtCuentaDestino = (EditText)root.findViewById(R.id.edt_cuentadestino_ATVRegistroT);
        edtCantidad = (EditText)root.findViewById(R.id.edt_cantidad_ATVRegistroT);
        btnRegistro = (Button) root.findViewById(R.id.btn_Registrar_ATVRegistroT);
        btnDatePicker.setOnClickListener(this);
        btnTimePicker.setOnClickListener(this);
        btnRegistro.setOnClickListener(this);
        return root;
    }

    @Override
    public void onClick(View view) {
        if (view == btnDatePicker) {

            // Get Current Date
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);


            DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {

                           // txtDate.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                            txtFecha.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
                        }
                    }, mYear, mMonth, mDay);
            datePickerDialog.show();
        }
        if (view == btnTimePicker) {

            // Get Current Time
            final Calendar c = Calendar.getInstance();
            mHour = c.get(Calendar.HOUR_OF_DAY);
            mMinute = c.get(Calendar.MINUTE);

            // Launch Time Picker Dialog
            TimePickerDialog timePickerDialog = new TimePickerDialog(getContext(),
                    new TimePickerDialog.OnTimeSetListener() {

                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay,
                                              int minute) {
                            String strmin = ""+minute;
                            if(strmin.length() == 1) {
                                txtTiempo.setText(hourOfDay + ":0" + minute + ":00");
                            }else{
                                txtTiempo.setText(hourOfDay + ":" + minute + ":00");
                            }

                        }
                    }, mHour, mMinute, false);
            timePickerDialog.show();
        }
        if(view == btnRegistro){
            btnRegistro.setEnabled(false);
            Thread threadVerificar = new Thread(new Runnable() {
                Handler handlerVerificar = new Handler();
                @Override
                public void run() {
                    objMovimiento = new Movimiento();
                    strVerificarCuenta = objMovimiento.Registrar_Movimiento(edtCuentaOrigen.getText().toString(), edtCuentaDestino.getText().toString(), Integer.parseInt(edtCantidad.getText().toString()),txtFecha.getText().toString(), txtTiempo.getText().toString());
                    //strVerificarCuenta = "null";
                    handlerVerificar.post(new Runnable() {
                        @Override
                        public void run() {
                            if(strVerificarCuenta.equals("null")) {
                                btnRegistro.setEnabled(true);
                                Toast.makeText(getContext(), "Error al registrar Transferencia",
                                        Toast.LENGTH_SHORT).show();
                            }else {
                                Toast.makeText(getContext(), "Registrado",
                                        Toast.LENGTH_SHORT).show();
                                edtCuentaOrigen.setText("");
                                edtCuentaDestino.setText("");
                                edtCantidad.setText("");
                                txtTiempo.setText("");
                                txtFecha.setText("");
                                btnRegistro.setEnabled(true);
                            }
                        }
                    });

                }
            });threadVerificar.start();


        }
    }


}
