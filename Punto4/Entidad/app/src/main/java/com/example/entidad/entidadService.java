package com.example.entidad;

import android.app.IntentService;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Binder;
import android.os.IBinder;


public class entidadService extends Service {
    private String strVerificarInternet = "Verifica su conexion";
    private Intent intentService;
    private String strLatitud, strLongitud;
    private final IBinder imBinder = new entidadBinder();




    public class entidadBinder extends Binder {
        public entidadService getService(){
            return entidadService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return imBinder;
    }



    public void Enviar_Informacion(int opcion){
        switch (opcion){
            case 1:
                intentService = new Intent("ActualizarPosicionPropia");
                intentService.putExtra("Latitud", strLatitud);
                intentService.putExtra("Longitud", strLongitud);
                sendBroadcast(intentService);
                break;
            case 2:

                break;
        }


    }
}
