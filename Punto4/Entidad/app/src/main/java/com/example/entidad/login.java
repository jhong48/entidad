package com.example.entidad;

import Logica.Usuario;
import androidx.appcompat.app.AppCompatActivity;
import com.google.android.material.snackbar.Snackbar;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

public class login extends AppCompatActivity implements Button.OnClickListener {
    private EditText edtClave, edtContrasenia;
    private Button btnIniciarSesion, btnRegistro;
    protected Usuario objUsuario;
    protected List<Datos.Usuario> listUsuarios;
    protected boolean booleanSesion =false;
    String intId_Usuario ="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        edtClave = (EditText) findViewById(R.id.edt_usuario_ATVLogin);
        edtContrasenia = (EditText)findViewById(R.id.edt_contrasenia_ATVLogin);
        objUsuario = new Usuario();
        btnIniciarSesion = (Button)findViewById(R.id.btn_login_ATVLogin);
        btnRegistro = (Button)findViewById(R.id.btn_registro_ATVLogin);
        btnIniciarSesion.setOnClickListener(this);
        btnRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent registroIntent = new Intent(login.this, registroUsuario.class);
                login.this.startActivity(registroIntent);
                login.this.finish();
            }
        });

    }

    @Override
    public void onClick(View view) {
        if(edtClave.getText().equals("") && edtContrasenia.getText().equals("")){
            Snackbar.make(view, "Ingresa Todos los Campos", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
        }else{
            Iniciar_Sesion(view);
            btnIniciarSesion.setEnabled(false);
        }
    }

    public void Iniciar_Sesion(final View view){
        Thread threadLogin = new Thread(new Runnable() {
            @Override
            public void run() {
                listUsuarios = new ArrayList<Datos.Usuario>();
                listUsuarios = objUsuario.Iniciar_Sesion(edtClave.getText().toString(), edtContrasenia.getText().toString());

                if(""+listUsuarios.get(0).getId_Usuario() != null){
                    booleanSesion = objUsuario.Registrar_VariableSession(listUsuarios.get(0).getId_Usuario(), getApplicationContext());
                    if(booleanSesion == true){
                        Verificar_UsuarioPersona();
                    }else{
                        btnIniciarSesion.setEnabled(true);
                        Snackbar.make(view, "Error al almacenar variable", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                    }

                }else{
                    btnIniciarSesion.setEnabled(true);
                    Snackbar.make(view, "Error al inciari sesion", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();

                }
            }
        });threadLogin.start();
    }


    protected void Verificar_UsuarioPersona() {

        intId_Usuario = objUsuario.Verificar_UsuarioPersona(Integer.parseInt(objUsuario.Obtener_VariableSession(getApplicationContext())));
        Log.e("Hola Jhon ", "Esta es la verificacion"+intId_Usuario);
        if (intId_Usuario.equals("null")) {
                            Intent registroIntent = new Intent(login.this, registro.class);
                            login.this.startActivity(registroIntent);
                            login.this.finish();
        } else {
            Intent mainIntent = new Intent(login.this, menuPrincipal2.class);
            //Intent loginIntent = new Intent(MainActivity.this, registro.class);
            login.this.startActivity(mainIntent);
            login.this.finish();
        }



    }
}
