package com.example.entidad;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.entidad.ui.movimientos.movimientos;
import java.util.ArrayList;
import java.util.List;
import Datos.Movimiento;
import androidx.recyclerview.widget.RecyclerView;

public class adapterMovimientos extends RecyclerView.Adapter<adapterMovimientos.MyViewHolder> {
        private List<Movimiento> movimientoList;
        private Context contextReciclerView;
        private movimientos fragmentPrincipal;

        public adapterMovimientos(Context context){
            this.setContextReciclerView(context);
            setMovimientoList(new ArrayList<Movimiento>());
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_movimientos, parent,false);
            return new MyViewHolder(view);
        }



    @Override
        public void onBindViewHolder(final MyViewHolder holder, final int position) {
            Movimiento objMovimiento = getMovimientoList().get(position);
            holder.edtNombreDestino.setText("Cuenta de Origen "+objMovimiento.getObjCuentaDestino().getNumero_Cuenta());
            holder.edtNombreOrigen.setText("Cuenta Destino "+objMovimiento.getObjCuentaOrigen().getNumero_Cuenta());
            holder.edtCantidad.setText("Cantidad "+objMovimiento.getObjTransferencia().getCantidad());
            holder.edtFecha.setText("Fecha de Transaccion "+objMovimiento.getObjTransferencia().getFecha_Transferencia());
        //
        //            //hlder.imageviewMenu.setOnClickListener(new onMyClick(position,5,holder));
            holder.btnCancelar.setOnClickListener(new onMyClick(position, 1, holder));


        }


        @Override
        public int getItemViewType(int position) {
            return super.getItemViewType(position);
        }

        @Override
        public long getItemId(int position) {
            return super.getItemId(position);
        }

        @Override
        public int getItemCount() {
            return getMovimientoList().size();
        }

    public List<Movimiento> getMovimientoList() {
        return movimientoList;
    }

    public void setMovimientoList(List<Movimiento> movimientoList) {
        this.movimientoList = movimientoList;
    }

    public Context getContextReciclerView() {
        return contextReciclerView;
    }

    public void setContextReciclerView(Context contextReciclerView) {
        this.contextReciclerView = contextReciclerView;
    }

    public movimientos getFragmentPrincipal() {
        return fragmentPrincipal;
    }

    public void setFragmentPrincipal(movimientos fragmentPrincipal) {
        this.fragmentPrincipal = fragmentPrincipal;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
            public TextView edtNombreOrigen, edtNombreDestino, edtCantidad, edtFecha;
            public Button btnCancelar;
            public MyViewHolder(View view) {
                super(view);
                edtNombreOrigen = (TextView)view.findViewById(R.id.edt_NombreOrigen_MovimientosAdapter);
                edtNombreDestino = (TextView)view.findViewById(R.id.edt_NombreDestino_MovimientosAdapter);
                edtCantidad = (TextView)view.findViewById(R.id.edt_Cantidad_MovimientosAdapter);
                edtFecha = (TextView) view.findViewById(R.id.edt_Fecha_MovimientosAdapter);
                btnCancelar = (Button)view.findViewById(R.id.btn_Cancelar_MovimientosAdapter);
            }
        }



        public class onMyClick implements View.OnClickListener {

            private final int pos;
            private final int opcion;
            private final MyViewHolder holder;
            public onMyClick(int pos, int op, MyViewHolder holder) {
                this.pos = pos;
                this.opcion = op;
                this.holder = holder;
            }

            @Override
            public void onClick(View v) {
                final Movimiento objMovimiento = getMovimientoList().get(pos);
                switch (this.opcion){
                    case 1:
                        //getFragmentPrincipal().imagepos(objNoticias.getId_Noticia());
                        getFragmentPrincipal().Cancelar_Transferencia(objMovimiento.getObjTransferencia().getId_Transferencia());
                       // getFragmentPrincipal().Listar_Movimientos();
                        holder.btnCancelar.setClickable(false);
                        break;
                    case 2:
                       // getFragmentPrincipal().Registrar_Gustar(objNoticias.getId_Noticia());
                        //holder.imageButtonGustar.setImageBitmap(bitmapGustarN);
                        break;
                    case 3:

                        break;

                }
            }
        }


}
