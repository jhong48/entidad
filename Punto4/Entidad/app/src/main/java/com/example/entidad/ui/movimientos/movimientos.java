package com.example.entidad.ui.movimientos;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import Datos.Movimiento;
import Logica.Usuario;
import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.entidad.R;
import com.example.entidad.adapterMovimientos;
import com.example.entidad.registro;

import java.util.ArrayList;
import java.util.List;

public class movimientos extends Fragment {
    private adapterMovimientos movimientosAdapter;
    protected List<Movimiento> movimientoList;
    public RecyclerView rcvMovimientos;
    private SendViewModel sendViewModel;
    protected Usuario obUsuario;
    protected Logica.Movimiento objMovimiento;
    String strResult ="";


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        sendViewModel =
                ViewModelProviders.of(this).get(SendViewModel.class);
        View root = inflater.inflate(R.layout.fragment_movimientos, container, false);
        rcvMovimientos = (RecyclerView) root.findViewById(R.id.rcv_movimientos_frgMovimientos);
        movimientosAdapter = new adapterMovimientos(getContext().getApplicationContext());
        movimientosAdapter.setFragmentPrincipal(this);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        final GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 6);
        rcvMovimientos.setLayoutManager(layoutManager);
        rcvMovimientos.setItemAnimator(new DefaultItemAnimator());

        Listar_Movimientos();
        return root;
    }

    public void Listar_Movimientos(){
        Thread threadNoticias = new Thread(){
            Handler handlerMovimientos = new Handler();
            @Override
            public void run(){
                obUsuario = new Usuario();
                objMovimiento = new Logica.Movimiento();
                movimientoList = new ArrayList<Movimiento>();
                movimientoList = objMovimiento.Listar_Movimientos(obUsuario.Obtener_VariableSession(getContext()));
                handlerMovimientos.post(new Runnable() {
                    @Override
                    public void run() {
                        if(movimientoList == null){
                            Toast.makeText(getContext(), "No tienes Transacciones Registradas",
                                    Toast.LENGTH_LONG).show();
                        }else{
                            movimientosAdapter.setMovimientoList(movimientoList);
                            rcvMovimientos.setAdapter(movimientosAdapter);
                            movimientosAdapter.notifyDataSetChanged();
                            //recyclerViewOtros.setAdapter(noticiasAdapter);
                        }
                    }
                });
            }
        }; threadNoticias.start();
    }

    public void Cancelar_Transferencia(final int intId_Movimiento){
        Thread threadNoticias = new Thread(){
            Handler handlerMovimientos = new Handler();
            @Override
            public void run(){
                String strIdTransaccion = ""+intId_Movimiento;
                objMovimiento = new Logica.Movimiento();
                strResult = objMovimiento.Cancelar_Transaccion(strIdTransaccion, "Inactivo");
                handlerMovimientos.post(new Runnable() {
                    @Override
                    public void run() {
                        if(strResult.equals("3") || strResult.equals("null")){
                            Toast.makeText(getContext(), "Error al cancelar la solicitud",
                                    Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(getContext(), "Transferencia Cancelada",
                                    Toast.LENGTH_SHORT).show();

                        }
                    }
                });
            }
        }; threadNoticias.start();

    }

    @Override
    public void onStop() {
        super.onStop();
        if(movimientoList != null) {
            movimientoList.clear();
        }
    }
    @Override
    public void onPause() {
        super.onPause();
        if(movimientoList != null) {
            movimientoList.clear();
        }

    }
}