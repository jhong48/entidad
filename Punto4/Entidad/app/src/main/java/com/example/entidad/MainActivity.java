package com.example.entidad;

import Logica.Usuario;
import androidx.appcompat.app.AppCompatActivity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

public class MainActivity extends AppCompatActivity {
    public entidadService eService;
    protected Usuario objUsuarios;
    String intId_Usuario ="";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        startService(new Intent(MainActivity.this, entidadService.class));
        objUsuarios = new Usuario();
        Obtener_VariableSesion();
    }




    protected void Obtener_VariableSesion() {

        Thread threadVerificarSesion = new Thread() {
            @Override
            public void run() {
                String intId_Usuario = objUsuarios.Obtener_VariableSession(getApplicationContext());
                if (intId_Usuario != "") {
                    Verificar_UsuarioPersona();
                } else {
                    Intent loginIntent = new Intent(MainActivity.this, login.class);
                    //Intent loginIntent = new Intent(MainActivity.this, registro.class);
                    MainActivity.this.startActivity(loginIntent);
                    MainActivity.this.finish();
                }

            }

        };
        threadVerificarSesion.start();
    }


        protected void Verificar_UsuarioPersona() {

            Thread threadVerificarSesion = new Thread() {
                @Override
                public void run() {
                    intId_Usuario = objUsuarios.Verificar_UsuarioPersona(Integer.parseInt(objUsuarios.Obtener_VariableSession(getApplicationContext())));

                           Log.e("Hola Jhon ", "Esta es la verificacion"+intId_Usuario);
                           if (intId_Usuario.equals("null")) {
                               Intent registroIntent = new Intent(MainActivity.this, registro.class);
                               MainActivity.this.startActivity(registroIntent);
                               MainActivity.this.finish();
                           } else {
                               Intent mainIntent = new Intent(MainActivity.this, menuPrincipal2.class);
                               //Intent loginIntent = new Intent(MainActivity.this, registro.class);
                               MainActivity.this.startActivity(mainIntent);
                               MainActivity.this.finish();
                           }




                }

            };
            threadVerificarSesion.start();

        }
}
