package com.example.entidad;

import Logica.Cuenta;
import Logica.Usuario;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;

public class registroUsuario extends AppCompatActivity {
    protected EditText edtNombres, edtApellidos, edtCorreo, edtCuenta, edtClave, edtContra;
    protected Button btnRegistrarCuenta, btnRegistrar;
    protected Usuario objUsuario;
    String intRegistroUsuario = "";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_usuario);
        edtClave = (EditText)findViewById(R.id.edt_clave_ATVRegistroUsuario);
        edtContra = (EditText)findViewById(R.id.edt_contrasenia_ATVRegistroUsuario);
        btnRegistrar = (Button)findViewById(R.id.btn_Registrar_ATVRegistroUsuario);
        btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    if(edtClave.getText().toString().equals("") || edtContra.getText().toString().equals("") ){
                        Snackbar.make(view, "Debe Ingresar Todos Los Campos", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                    }else{
                        btnRegistrar.setEnabled(false);
                        objUsuario = new Usuario();
                        Registrar_Usuario();
                    }
                }
        });

    }


    public void Registrar_Usuario(){
        Thread threadRegistrar = new Thread(new Runnable() {
            Handler handlerRegistrar = new Handler();
            @Override
            public void run() {
                intRegistroUsuario = objUsuario.Registrar_Usuario(edtClave.getText().toString(), edtContra.getText().toString());
                handlerRegistrar.post(new Runnable() {
                    @Override
                    public void run() {
                        Log.e("Hola Jhon" ,"resultado "+intRegistroUsuario);
                        if(intRegistroUsuario == null){
                            btnRegistrar.setEnabled(true);
                            Toast.makeText(registroUsuario.this, "Intenta con otra Clave",
                                    Toast.LENGTH_SHORT).show();
                        }else{
                            objUsuario.Registrar_VariableSession(Integer.parseInt(intRegistroUsuario), getApplicationContext());
                            Intent registroIntent = new Intent(registroUsuario.this, registro.class);
                            registroUsuario.this.startActivity(registroIntent);
                            registroUsuario.this.finish();
                        }
                    }
                });
            }
        });threadRegistrar.start();
    }


}
