package com.example.entidad;

import Logica.Cuenta;
import Logica.Usuario;
import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;
import com.google.android.material.snackbar.Snackbar;
import java.util.ArrayList;

public class registro extends AppCompatActivity {
    protected EditText edtNombres, edtApellidos, edtCorreo, edtCuenta;
    protected Button btnRegistrarCuenta, btnRegistrar;
    protected CheckBox chbTerminos;
    private ListView listCuentas;
    private ArrayAdapter<String> adapter;
    private ArrayList<String> arrayListCuentas;
    protected Usuario objUsuario;
    protected Cuenta objCuenta;
    String strVerificarCuenta = "";
    String intRegistroUsuario = "";
    String strRegistroCuenta = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
        edtNombres = (EditText)findViewById(R.id.edt_nombres_ATVRegistro);
        edtApellidos = (EditText)findViewById(R.id.edt_apellidos_ATVRegistro);
        edtCorreo = (EditText)findViewById(R.id.edt_correo_ATVRegistro);
        edtCuenta = (EditText)findViewById(R.id.edt_cuenta_ATVRegistro);
        btnRegistrarCuenta = (Button)findViewById(R.id.btn_registrarcuenta_ATVRegistro);
        btnRegistrar = (Button)findViewById(R.id.btn_Registrar_ATVRegistro);
        listCuentas = (ListView) findViewById(R.id.lt_listacuenta_ATVRegistro);
        chbTerminos = (CheckBox) findViewById(R.id.chb_aceptarTer_ATVRegistro);
        arrayListCuentas = new ArrayList<String>();

        adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_item, arrayListCuentas);

        btnRegistrarCuenta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(edtCuenta.getText().equals("")) {
                    Toast.makeText(getApplicationContext(), "Ingrese Cuenta",
                            Toast.LENGTH_LONG).show();
                }else{
                     Verificar_Cuenta();
                   // String strResult = "null";


                }

            }
        });
        btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(arrayListCuentas.size() == 0){
                    Toast.makeText(getApplicationContext(), "Debe Ingresar Al Menos Una Cuenta",
                            Toast.LENGTH_LONG).show();
                }else{
                    if(chbTerminos.isChecked() == false){
                        Toast.makeText(getApplicationContext(), "Debe Aceptar Terminos y Condiciones",
                                Toast.LENGTH_LONG).show();
                    }

                    if(edtNombres.getText().toString().equals("") || edtApellidos.getText().toString().equals("") || edtCorreo.getText().toString().equals("")){
                        Toast.makeText(getApplicationContext(), "Debe Ingresar Todos Los Campos",
                                Toast.LENGTH_LONG).show();
                    }else{
                        btnRegistrar.setEnabled(false);
                        objUsuario = new Usuario();
                        Registrar_Usuario();

                    }
                }
            }
        });
        listCuentas.setAdapter(adapter);

    }

    public void Verificar_Cuenta(){
        Thread threadVerificar = new Thread(new Runnable() {
            Handler handlerVerificar = new Handler();
            @Override
            public void run() {
                objCuenta = new Cuenta();
               strVerificarCuenta = objCuenta.Verificar_Cuenta(edtCuenta.getText().toString());
                //strVerificarCuenta = "null";
                handlerVerificar.post(new Runnable() {
                   @Override
                   public void run() {
                       Log.e("Hola Jhon ", "este es"+strVerificarCuenta);
                       if(strVerificarCuenta.equals("null")) {
                           arrayListCuentas.add(edtCuenta.getText().toString());
                           edtCuenta.setText("");
                           adapter.notifyDataSetChanged();
                       }else {
                           edtCuenta.setText("");
                       }
                   }
               });

            }
        });threadVerificar.start();

    }

    public void Registrar_Usuario(){
        Thread threadRegistrar = new Thread(new Runnable() {
            Handler handlerRegistrar = new Handler();
            @Override
            public void run() {
                intRegistroUsuario = objUsuario.Registrar_Persona(edtNombres.getText().toString(), edtApellidos.getText().toString(), edtCorreo.getText().toString(), Integer.parseInt(objUsuario.Obtener_VariableSession(getApplicationContext())));

                handlerRegistrar.post(new Runnable() {
                    @Override
                    public void run() {
                        Log.e("Hola Jhon" ,"resultado "+intRegistroUsuario);
                        if(intRegistroUsuario.equals(""+objUsuario.Obtener_VariableSession(getApplicationContext()))){
                            for (int x = 0; x< arrayListCuentas.size(); x++){
                                Registrar_Cuenta(x);
                            }
                        }else{
                            btnRegistrar.setEnabled(true);
                            Toast.makeText(registro.this, "Error al registrar Usuario",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });threadRegistrar.start();
    }

    public void Registrar_Cuenta(final int x){
        Thread threadRegistrar = new Thread(new Runnable() {
            Handler handlerRegistrar = new Handler();
            @Override
            public void run() {
                strRegistroCuenta = objCuenta.Registrar_Cuenta(arrayListCuentas.get(x), Integer.parseInt(objUsuario.Obtener_VariableSession(getApplicationContext())));
                handlerRegistrar.post(new Runnable() {
                    @Override
                    public void run() {
                        if(strRegistroCuenta.equals("notnull")){
                            Toast.makeText(getApplicationContext(), "Registrado",
                                    Toast.LENGTH_SHORT).show();
                            Intent mainIntent = new Intent(registro.this, menuPrincipal2.class);
                            //Intent loginIntent = new Intent(MainActivity.this, registro.class);
                            registro.this.startActivity(mainIntent);
                            registro.this.finish();
                        }else{
                            btnRegistrar.setEnabled(true);
                            Toast.makeText(registro.this, "Error al registrar Cuenta",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });threadRegistrar.start();

    }




}
