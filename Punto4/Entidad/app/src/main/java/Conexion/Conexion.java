package Conexion;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;


public class Conexion {
    public String strURLServidor = "https://entidadprueba.000webhostapp.com/entidad/";
    //public String strURLServidor = "http://192.168.43.254/entidad/";

    public JSONObject HttpRequest(String strURL){
        String json = "";
        JSONObject jObj = null;
        HttpURLConnection urlConnection = null;
        String jsonString = new String();
        StringBuilder sb = null;
        Log.e("Hola Jhon ", "esta es url "+strURLServidor+""+strURL);
        try {
            URL url = new URL(strURLServidor+""+strURL);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.setReadTimeout(10000 /* milliseconds */);
            urlConnection.setConnectTimeout(15000 /* milliseconds */);
            urlConnection.setDoOutput(true);
            urlConnection.connect();
            BufferedReader br=new BufferedReader(new InputStreamReader(url.openStream()));
            char[] buffer = new char[1024];
            sb = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line+"\n");
            }
            br.close();


        } catch (IOException e) {
            e.printStackTrace();
        }
        if (sb != null) {
            jsonString = sb.toString();
            try {
                jObj = new JSONObject(jsonString);
            } catch (JSONException e) {
                Log.e("JSON Parser", "Error parsing data " + e.toString());
            }

            // return JSON String
        }else {
            jObj = null;
        }

        return jObj;

    }

    public String HttpRequestString(String strURL){
        HttpURLConnection urlConnection = null;
        StringBuilder sb = null;
        String line ="";

        try {
            URL url = new URL(strURLServidor+""+strURL);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.setReadTimeout(10000 /* milliseconds */);
            urlConnection.setConnectTimeout(15000 /* milliseconds */);
            urlConnection.setDoOutput(true);
            urlConnection.connect();
            BufferedReader br=new BufferedReader(new InputStreamReader(url.openStream()));
            sb = new StringBuilder();

            while ((line = br.readLine()) != null) {

                sb.append(line);
            }

            br.close();


        } catch (IOException e) {
            e.printStackTrace();
        }
        return ""+sb;
    }

}
