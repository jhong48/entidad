package Datos;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import Conexion.Conexion;

public class Movimiento {
    private int Id_Movimento;
    private Cuenta objCuentaOrigen;
    private Cuenta objCuentaDestino;
    private Transferencia objTransferencia;


    public Movimiento(){
        this.setId_Movimento(0);
        this.setObjCuentaOrigen(null);
        this.setObjCuentaDestino(null);
        this.objTransferencia = null;
    }

    public Movimiento(int intId_Movimiento, Cuenta objCuentaOrigen, Cuenta objCuentaDestino, Transferencia objTransferencia){
        this.setId_Movimento(intId_Movimiento);
        this.setObjCuentaOrigen(objCuentaOrigen);
        this.setObjCuentaDestino(objCuentaDestino);
        this.setObjTransferencia(objTransferencia);

    }


    public int getId_Movimento() {
        return Id_Movimento;
    }

    public void setId_Movimento(int id_Movimento) {
        Id_Movimento = id_Movimento;
    }

    public Cuenta getObjCuentaOrigen() {
        return objCuentaOrigen;
    }

    public void setObjCuentaOrigen(Cuenta objCuentaOrigen) {
        this.objCuentaOrigen = objCuentaOrigen;
    }

    public Cuenta getObjCuentaDestino() {
        return objCuentaDestino;
    }

    public void setObjCuentaDestino(Cuenta ObjCuentaDestino) {
        this.objCuentaDestino = ObjCuentaDestino;
    }


    public String Registrar_Movimiento(Movimiento objMovimiento){
        String strIdReturn = "";
        String strURL = "index_cuentas.php?opcion=Registrar_Movimiento&Numero_CuentaOr="+objMovimiento.getObjCuentaOrigen().getNumero_Cuenta()+"&Numero_CuentaDest="+objMovimiento.getObjCuentaDestino().getNumero_Cuenta()+"&Cantidad="+objMovimiento.getObjTransferencia().getCantidad()+"&Fecha_Transferencia="+objMovimiento.getObjTransferencia().getFecha_Transferencia()+"&Hora_Transferencia="+objMovimiento.getObjTransferencia().getHora_Transferencia();
        Conexion con = new Conexion();
        JSONObject jsonObjectListarUsuarios = con.HttpRequest(strURL);
        if(jsonObjectListarUsuarios == null){
            return "null";
        }else {
            try {
                JSONArray jsonArrayNoticias = jsonObjectListarUsuarios.getJSONArray("Listar_Movimiento");
                for (int i = 0; i < jsonArrayNoticias.length(); i++) {
                    JSONObject jsonObjectFeed = (JSONObject) jsonArrayNoticias.get(i);
                    strIdReturn = jsonObjectFeed.getString("Id_Movimiento");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return strIdReturn;
        }
    }

    public List<Movimiento> Listar_Movimientos(String strId_Usuario){
        List<Movimiento> listMovimiento = new ArrayList<Movimiento>();
        final String url_noticias = "index_cuentas.php?opcion=Listar_Movimientos&Id_Usuario="+strId_Usuario;
        Conexion con = new Conexion();
        JSONObject jsonObjectListarNoticias = con.HttpRequest(url_noticias);
        if(jsonObjectListarNoticias == null){
            return null;
        }else {
            try {
                JSONArray jsonArrayNoticias = jsonObjectListarNoticias.getJSONArray("Listar_Movimientos");
                for (int i = 0; i < jsonArrayNoticias.length(); i++) {
                    Datos.Movimiento objMovimientos = new Datos.Movimiento();
                    Cuenta objCuentaOrigen = new Cuenta();
                    Cuenta objCuentaDestino = new Cuenta();
                    Transferencia objTransferencia = new Transferencia();
                    JSONObject jsonObjectFeed = (JSONObject) jsonArrayNoticias.get(i);
                    objTransferencia.setId_Transferencia(jsonObjectFeed.getInt("Id_transferencia"));
                    objCuentaOrigen.setNumero_Cuenta(jsonObjectFeed.getString("Id_CuentaOrigen"));
                    objCuentaDestino.setNumero_Cuenta(jsonObjectFeed.getString("Id_CuentaDestino"));
                    objTransferencia.setEstado(jsonObjectFeed.getString("Estado"));
                    objTransferencia.setCantidad(jsonObjectFeed.getInt("Cantidad_Transferencia"));
                    objTransferencia.setFecha_Transferencia(jsonObjectFeed.getString("Fecha_Transferencia"));
                    objMovimientos.setObjCuentaOrigen(objCuentaOrigen);
                    objMovimientos.setObjCuentaDestino(objCuentaDestino);
                    objMovimientos.setObjTransferencia(objTransferencia);
                    listMovimiento.add(objMovimientos);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return listMovimiento;
        }
    }

    public String Cancelar_Transaccion(Movimiento objMovimiento){
        String strIdReturn = "";
        String strURL = "index_cuentas.php?opcion=Cambiar_Estado&Id_Transferencia="+objMovimiento.getObjTransferencia().getId_Transferencia()+"&Estado="+objMovimiento.getObjTransferencia().getEstado();
        Conexion con = new Conexion();
        JSONObject jsonObjectListarUsuarios = con.HttpRequest(strURL);
        if(jsonObjectListarUsuarios == null){
            return "null";
        }else {
            try {
                JSONArray jsonArrayNoticias = jsonObjectListarUsuarios.getJSONArray("Cambiar_Estado");
                for (int i = 0; i < jsonArrayNoticias.length(); i++) {
                    JSONObject jsonObjectFeed = (JSONObject) jsonArrayNoticias.get(i);
                    strIdReturn = jsonObjectFeed.getString("Id_Transferencia");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return strIdReturn;
        }
    }

    public Transferencia getObjTransferencia() {
        return objTransferencia;
    }

    public void setObjTransferencia(Transferencia objTransferencia) {
        this.objTransferencia = objTransferencia;
    }
}
