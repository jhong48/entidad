package Datos;

import android.content.Context;
import android.content.SharedPreferences;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;
import Conexion.Conexion;

public class Usuario extends Persona {
    private int Id_Usuario;
    private String Clave;
    private String Contrasenia;

    public Usuario(){
        this.Id_Usuario = 0;
        this.Clave = "";
        this.Contrasenia = "";
    }

    public Usuario(int intId_Persona, String strNombres, String strApellidos, String strCorreo, int intId_Usuario, String strClave, String strContrasenia){
        this.Id_Usuario = intId_Usuario;
        this.Clave = strClave;
        this.Contrasenia = strContrasenia;
        super.setId_Persona(intId_Persona);
        super.setNombres(strNombres);
        super.setApellidos(strApellidos);
        super.setCorreo(strCorreo);
    }

    public int getId_Usuario() {
        return Id_Usuario;
    }

    public void setId_Usuario(int id_Usuario) {
        Id_Usuario = id_Usuario;
    }

    public String getClave() {
        return Clave;
    }

    public void setClave(String clave) {
        Clave = clave;
    }

    public String getContrasenia() {
        return Contrasenia;
    }

    public void setContrasenia(String contrasenia) {
        Contrasenia = contrasenia;
    }

    public String Registrar_Persona(Usuario objUsuarios){
        List<Usuario> listUsuarios = new ArrayList<Usuario>();
        String strIdReturn = "";
        String strURL = "index_usuarios.php?opcion=Registrar_Persona&Nombres="+objUsuarios.getNombres()+"&Apellidos="+objUsuarios.getApellidos()+"&Correo="+objUsuarios.getCorreo()+"&Id_Usuario="+objUsuarios.getId_Usuario();
        Conexion con = new Conexion();
        JSONObject jsonObjectListarUsuarios = con.HttpRequest(strURL);
        if(jsonObjectListarUsuarios.equals(null)){
            return null;
        }else {
            try {
                JSONArray jsonArrayNoticias = jsonObjectListarUsuarios.getJSONArray("Listar_Usuario");
                for (int i = 0; i < jsonArrayNoticias.length(); i++) {
                    JSONObject jsonObjectFeed = (JSONObject) jsonArrayNoticias.get(i);
                    strIdReturn = jsonObjectFeed.getString("Id_Usuario");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return strIdReturn;
        }
    }


    public String Registrar_Usuario(Usuario objUsuarios){
        List<Usuario> listUsuarios = new ArrayList<Usuario>();
        String strIdReturn = "";
        String strURL = "index_usuarios.php?opcion=Registrar_Usuario&Clave="+objUsuarios.getClave()+"&Contrasenia="+objUsuarios.getContrasenia();
        Conexion con = new Conexion();
        JSONObject jsonObjectListarUsuarios = con.HttpRequest(strURL);
        if(jsonObjectListarUsuarios ==null){
            return null;
        }else {
            try {
                JSONArray jsonArrayNoticias = jsonObjectListarUsuarios.getJSONArray("Listar_Usuario");
                for (int i = 0; i < jsonArrayNoticias.length(); i++) {
                    JSONObject jsonObjectFeed = (JSONObject) jsonArrayNoticias.get(i);
                    strIdReturn = jsonObjectFeed.getString("Id_Usuario");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return strIdReturn;
        }
    }


    public List<Usuario> Iniciar_Sesion(Usuario objUsuario){
        List<Usuario> listUsuarios = new ArrayList<Usuario>();
        String url_noticias = "index_usuarios.php?opcion=Iniciar_Sesion&Clave="+objUsuario.getClave()+"&Contrasenia="+objUsuario.getContrasenia();
        Conexion con = new Conexion();
        JSONObject jsonObjectListarUsuarios = con.HttpRequest(url_noticias);
        if(jsonObjectListarUsuarios == null){
            return null;
        }else {
            try {
                JSONArray jsonArrayNoticias = jsonObjectListarUsuarios.getJSONArray("Iniciar_Sesion");
                for (int i = 0; i < jsonArrayNoticias.length(); i++) {
                    JSONObject jsonObjectFeed = (JSONObject) jsonArrayNoticias.get(i);
                    objUsuario.setId_Usuario(jsonObjectFeed.getInt("Id_Usuario"));
                    listUsuarios.add(objUsuario);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return listUsuarios;
        }
    }


    public boolean Registrar_VarialbleSesion(Context context, Usuario objUsuario){
        SharedPreferences.Editor variableSesion = context.getSharedPreferences("entidad", Context.MODE_PRIVATE).edit();
        variableSesion.putString("Id_Udsuario", ""+objUsuario.Id_Usuario);
        variableSesion.commit();
        return true;
    }

    public String Obtener_VariableSesion(Context context){
        SharedPreferences sharedPreferencesSesion = context.getSharedPreferences("entidad",	Context.MODE_PRIVATE);
        return sharedPreferencesSesion.getString("Id_Udsuario", "");
    }

    public String Verificar_CuentaPersona(Usuario objUsuario){
        String strIdReturn = "";
        String strURL = "index_usuarios.php?opcion=Verificar_UsuarioPersona&Id_Usuario="+objUsuario.getId_Usuario();
        Conexion con = new Conexion();
        JSONObject jsonObjectListarUsuarios = con.HttpRequest(strURL);
        if(jsonObjectListarUsuarios == null){
            return "null";
        }else {
            try {
                JSONArray jsonArrayNoticias = jsonObjectListarUsuarios.getJSONArray("Verificar_UsuarioPersona");
                for (int i = 0; i < jsonArrayNoticias.length(); i++) {
                    JSONObject jsonObjectFeed = (JSONObject) jsonArrayNoticias.get(i);
                    strIdReturn = jsonObjectFeed.getString("Id_Persona");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return strIdReturn;
        }
    }

}
