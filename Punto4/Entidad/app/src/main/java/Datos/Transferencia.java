package Datos;

public class Transferencia {
    private int Id_Transferencia;
    private int Cantidad;
    private String Fecha_Transferencia;
    private String Hora_Transferencia;
    private String Estado;

    public Transferencia(){
        this.setId_Transferencia(0);
        this.setCantidad(0);
        this.setFecha_Transferencia("");
        this.setHora_Transferencia("");
        this.setEstado("");
    }

    public Transferencia(int intId_Transferencia, int intCantidad, String strFecha_Transferencia, String strHora_Transferencia, String strEstado){
        this.setId_Transferencia(intId_Transferencia);
        this.setCantidad(intCantidad);
        this.setHora_Transferencia(strHora_Transferencia);
        this.setFecha_Transferencia(strFecha_Transferencia);
        this.setEstado(strEstado);
    }
    public int getCantidad() {
        return Cantidad;
    }

    public void setCantidad(int cantidad) {
        Cantidad = cantidad;
    }

    public String getFecha_Transferencia() {
        return Fecha_Transferencia;
    }

    public void setFecha_Transferencia(String fecha_Transferencia) {
        Fecha_Transferencia = fecha_Transferencia;
    }

    public String getHora_Transferencia() {
        return Hora_Transferencia;
    }

    public void setHora_Transferencia(String hora_Transferencia) {
        Hora_Transferencia = hora_Transferencia;
    }

    public int getId_Transferencia() {
        return Id_Transferencia;
    }

    public void setId_Transferencia(int id_Transferencia) {
        Id_Transferencia = id_Transferencia;
    }

    public String getEstado() {
        return Estado;
    }

    public void setEstado(String estado) {
        Estado = estado;
    }
}
