package Datos;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import Conexion.Conexion;

public class Cuenta {
    private int Id_Cuenta;
    private String Numero_Cuenta;
    private String Estado;
    private Usuario objUsuario;

    public Cuenta(){
        this.Id_Cuenta = 0;
        this.Numero_Cuenta = "";
        this.Estado = "";
        this.objUsuario = null;
    }

    public Cuenta(int intId_Cuenta, String strNumero_Cuenta, String strEstado, Usuario objUsuarios){
        this.Id_Cuenta = intId_Cuenta;
        this.Numero_Cuenta = strNumero_Cuenta;
        this.Estado = strEstado;
        this.objUsuario = objUsuarios;
    }

    public int getId_Cuenta() {
        return Id_Cuenta;
    }

    public void setId_Cuenta(int id_Cuenta) {
        Id_Cuenta = id_Cuenta;
    }

    public String getNumero_Cuenta() {
        return Numero_Cuenta;
    }

    public void setNumero_Cuenta(String numero_Cuenta) {
        Numero_Cuenta = numero_Cuenta;
    }

    public String getEstado() {
        return Estado;
    }

    public void setEstado(String estado) {
        Estado = estado;
    }

    public Usuario getObjUsuario() {
        return objUsuario;
    }

    public void setObjUsuario(Usuario objUsuario) {
        this.objUsuario = objUsuario;
    }

    public String Registrar_Cuenta(Cuenta objCuenta){
        String strURL = "index_cuentas.php?opcion=Registrar_Cuenta&Numero_Cuenta="+objCuenta.getNumero_Cuenta()+"&Id_Usuario="+objCuenta.getObjUsuario().getId_Usuario();
        Conexion con = new Conexion();
        String intRes = con.HttpRequestString(strURL);
        return intRes;
    }

    public String Verificar_Cuenta(Cuenta objCuenta){
        String strURL = "index_cuentas.php?opcion=Verificar_Cuenta&Numero_Cuenta="+objCuenta.getNumero_Cuenta();
        Conexion con = new Conexion();
        String strRes = con.HttpRequestString(strURL);
        return strRes;
    }
}
