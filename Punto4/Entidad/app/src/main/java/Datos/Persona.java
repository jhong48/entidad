package Datos;

public abstract class Persona {
    private int Id_Persona;
    private String Nombres;
    private String Apellidos;
    private String Correo;


    public Persona(){
        this.setId_Persona(0);
        this.setNombres("");
        this.setApellidos("");
        this.setCorreo("");
    }

    public Persona(int intId_Persona, String strNombres, String strApellidos, String strCorreo){
        this.setId_Persona(intId_Persona);
        this.setNombres(strNombres);
        this.setApellidos(strApellidos);
        this.setCorreo(strCorreo);
    }

    public int getId_Persona() {
        return Id_Persona;
    }

    public void setId_Persona(int id_Persona) {
        Id_Persona = id_Persona;
    }

    public String getNombres() {
        return Nombres;
    }

    public void setNombres(String nombres) {
        Nombres = nombres;
    }

    public String getApellidos() {
        return Apellidos;
    }

    public void setApellidos(String apellidos) {
        Apellidos = apellidos;
    }

    public String getCorreo() {
        return Correo;
    }

    public void setCorreo(String correo) {
        Correo = correo;
    }

    public void setPersona(int intId_Persona, String strNombres, String strApellidos, String strCorreo){
        this.setId_Persona(intId_Persona);
        this.setNombres(strNombres);
        this.setApellidos(strApellidos);
        this.setCorreo(strCorreo);
    }
}
