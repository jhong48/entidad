<?php
include_once ("../entidad/modelo/modelo_cuenta.php");

	$opcion = isset($_REQUEST['opcion']) ? $_REQUEST['opcion'] : NULL;
	switch($opcion){
		
		case "Registrar_Cuenta":
			$model = new modelo_cuenta();
			$Numero_Cuenta = isset($_REQUEST['Numero_Cuenta']) ? $_REQUEST['Numero_Cuenta'] : NULL;
			$Id_Usuario = isset($_REQUEST['Id_Usuario']) ? $_REQUEST['Id_Usuario'] : NULL;
			$result = $model->Registrar_Cuenta($Numero_Cuenta, $Id_Usuario);
			echo $result;
		break;
		
		case "Verificar_Cuenta":
			$model = new modelo_cuenta();
			$Numero_Cuenta = isset($_REQUEST['Numero_Cuenta']) ? $_REQUEST['Numero_Cuenta'] : NULL;
			$result = $model->Verificar_Cuenta($Numero_Cuenta);
			echo $result;
		break;
		
		case "Registrar_Movimiento":
			$model = new modelo_cuenta();
			$Numero_CuentaOr = isset($_REQUEST['Numero_CuentaOr']) ? $_REQUEST['Numero_CuentaOr'] : NULL;
			$Numero_CuentaDest = isset($_REQUEST['Numero_CuentaDest']) ? $_REQUEST['Numero_CuentaDest'] : NULL;
			$Cantidad = isset($_REQUEST['Cantidad']) ? $_REQUEST['Cantidad'] : NULL;
			$Fecha_Transferencia = isset($_REQUEST['Fecha_Transferencia']) ? $_REQUEST['Fecha_Transferencia'] : NULL;
			$Hora_Transferencia = isset($_REQUEST['Hora_Transferencia']) ? $_REQUEST['Hora_Transferencia'] : NULL;
			$result = $model->Registrar_Movimiento($Numero_CuentaOr, $Numero_CuentaDest, $Cantidad, $Fecha_Transferencia,$Hora_Transferencia);
			if($result != null){
				echo json_encode(array('Listar_Movimiento' => $result));
			}else{
				echo "result null";
			}
		break;
		
		case "Listar_Movimientos":
			$model = new modelo_cuenta();
			$Id_Usuario = isset($_REQUEST['Id_Usuario']) ? $_REQUEST['Id_Usuario'] : NULL;
			$result = $model->Listar_Movimientos($Id_Usuario);
			if($result != null){
				echo json_encode(array('Listar_Movimientos' => $result));
			}else{
				echo "null";
			}
		break;
		
		case "Cambiar_Estado":
			$model = new modelo_cuenta();
			$Id_Transferencia = isset($_REQUEST['Id_Transferencia']) ? $_REQUEST['Id_Transferencia'] : NULL;
			$Estado = isset($_REQUEST['Estado']) ? $_REQUEST['Estado'] : NULL;
			$result = $model->Cambiar_EstadoTransferencia($Id_Transferencia, $Estado);
			if($result != null){
				echo json_encode(array('Cambiar_Estado' => $result));
			}else{
				echo "null";
			}
		break;
	
		Default:
			echo "Opcion Invalida";
		break;
	}
	



?>