<?php
include_once ("../retame/modelo/modelo_materia.php");

	$opcion = isset($_REQUEST['opcion']) ? $_REQUEST['opcion'] : NULL;
	switch($opcion){
		
		case "Listar_Materias":	
			$resultListarMaterias = array();
			$model = new modelo_materias();
			$resultListarMaterias = $model->Listar_Materias("1");
	
			if($resultListarMaterias != null){
				echo json_encode(array('Listar_Materias' => $resultListarMaterias));
			}else{
				echo null;
			}	
		break;
		
		case "Listar_MateriasAd":	
			$resultListarMaterias = array();
			$model = new modelo_materias();
			$resultListarMaterias = $model->Listar_Materias("0");
			if($resultListarMaterias != null){
				echo json_encode(array('Listar_Materias' => $resultListarMaterias));
			}else{
				echo null;
			}	
		break;
		
		case "Buscar_Materia":	
			$strBusqueda = isset($_REQUEST['Busqueda']) ? $_REQUEST['Busqueda'] : NULL;
			$resultListarMaterias = array();
			$model = new modelo_materias();
			$resultListarMaterias = $model->Buscar_Materia($strBusqueda);
			if($resultListarMaterias != null){
				echo json_encode(array('Buscar_Materias' => $resultListarMaterias));
			}else{
				echo null;
			}	
		break;
		
		case "Registrar_Materia":
			$result = "1";
			$Nombre_Materia = isset($_REQUEST['Nombre_Materia']) ? $_REQUEST['Nombre_Materia'] : NULL;
			$Descripcion_Materia = isset($_REQUEST['Descripcion_Materia']) ? $_REQUEST['Descripcion_Materia'] : NULL;
			$model = new modelo_materias();
			$result = $model->Registrar_Materia($Nombre_Materia, $Descripcion_Materia);
			echo $result;
		break;
		
		case "Actualizar_Materia":
			$result = "1";
			$Id_Materia = isset($_REQUEST['Id_Materia']) ? $_REQUEST['Id_Materia'] : NULL;
			$Nombre_Materia = isset($_REQUEST['Nombre_Materia']) ? $_REQUEST['Nombre_Materia'] : NULL;
			$Descripcion_Materia = isset($_REQUEST['Descripcion_Materia']) ? $_REQUEST['Descripcion_Materia'] : NULL;
			$model = new modelo_materias();
			$result = $model->Actualizar_Materia($Id_Materia, $Nombre_Materia, $Descripcion_Materia);
			echo $result;
		break;
		
		case "Cargar_Materia":
			$Id_Materia = isset($_REQUEST['Id_Materia']) ? $_REQUEST['Id_Materia'] : NULL;
			$resultCargarMateria = array();
			$model = new modelo_materias();
			$resultCargarMateria = $model->Cargar_Materia($Id_Materia);
			if($resultCargarMateria != null){
				echo json_encode(array('Cargar_Materia' => $resultCargarMateria));
			}else{
				echo null;
			}	
		break;
		
		case "Cambiar_Estado":
			$result = "1";
			$Id_Materia = isset($_REQUEST['Id_Materia']) ? $_REQUEST['Id_Materia'] : NULL;
			$Estado_Materia = isset($_REQUEST['Estado_Materia']) ? $_REQUEST['Estado_Materia'] : NULL;
			$model = new modelo_materias();
			$result = $model->Cambiar_Estado($Id_Materia, $Estado_Materia);
			echo $result;
		break;
		
		Default:
			echo "Opcion Invalida";
		break;
	}
	



?>