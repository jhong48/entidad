<!DOCTYPE html>
<?php
error_reporting(E_ALL & ~E_NOTICE);
$lcIdPublicacion = null;
$lcNombrePublicacion = null;
$lcDescripcion = null;
$lcpdf = null;
$Nombre_Noticia;
$Descripcion;
$pdf;

if(!empty($_GET['lcOperacion'] && !empty($_GET['lcListo']))){
	$operacion = $_GET['lcOperacion'];
	$listo = $_GET['lcListo'];
	if($operacion=='buscar' && listo == 1){
		$lcIdPublicacion = $_GET['lcIdPublicacion'];
		$lcNombrePublicacion = $_GET['lcNombrePublicacion'];
		$lcDescripcion = $_GET['lcDescripcion'];
		$lcpdf = $_GET['lcpdf'];
		
	}
}

include_once("/modelo/modelo_noticias.php");

	$Id_Noticia = isset($_REQUEST['Id_Noticia']) ? $_REQUEST['Id_Noticia'] : NULL;
	$res = array();
	$model = new modelo_noticias();
	$res = $model->Cargar_Noticia($Id_Noticia);
		if($res != null){
			$pdf = $res[0]['pdf'];
			$Nombre_Noticia = $res[0]['Nombre_Noticia'];
			$Descripcion = $res[0]['Descripcion'];
		}
		else{
		echo "error";
		}				
?>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title></title>
    <link href="src/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="src/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">
    <link href="jumbotron-narrow.css" rel="stylesheet">
    <script src="src/assets/js/ie-emulation-modes-warning.js"></script>
  </head>
<script>
function cargar()
{
	alert('El Registro que Busca no Existe');
	buscar();
}
window.onload = buscar();
	</script>
	<script type="text/x-mathjax-config">
		  MathJax.HTML.Cookie.Set("menu",{});
		  MathJax.Hub.Config({
			  
			  
			extensions: ["tex2jax.js"],
			jax: ["input/TeX","output/HTML-CSS"],
			"HTML-CSS": {
			  availableFonts:[],
			  styles: {".MathJax_Preview": {visibility: "hidden"}}
			}
			
		  });
		  MathJax.Hub.Register.StartupHook("HTML-CSS Jax Ready",function () {
			var FONT = MathJax.OutputJax["HTML-CSS"].Font;
			FONT.loadError = function (font) {
			  MathJax.Message.Set("Can't load web font TeX/"+font.directory,null,2000);
			  document.getElementById("noWebFont").style.display = "";
			};
			FONT.firefoxFontError = function (font) {
			  MathJax.Message.Set("Firefox can't load web fonts from a remote host",null,3000);
			  document.getElementById("ffWebFont").style.display = "";
			};
		  });

		(function (HUB) {
		  
		  var MINVERSION = {
			Firefox: 3.0,
			Opera: 9.52,
			MSIE: 6.0,
			Chrome: 0.3,
			Safari: 2.0,
			Konqueror: 4.0,
			Unknown: 10000.0 // always disable unknown browsers
		  };
		  
		  if (!HUB.Browser.versionAtLeast(MINVERSION[HUB.Browser]||0.0)) {
			HUB.Config({
			  jax: [],                   // don't load any Jax
			  extensions: [],            // don't load any extensions
			  "v1.0-compatible": false   // skip warning message due to no jax
			});
			setTimeout('document.getElementById("badBrowser").style.display = ""',0);
		  }
		  
		})(MathJax.Hub);

		MathJax.Hub.Register.StartupHook("End",function () {
		  var HTMLCSS = MathJax.OutputJax["HTML-CSS"];
		  if (HTMLCSS && HTMLCSS.imgFonts) {document.getElementById("imageFonts").style.display = ""}
		});

	</script>
	<script type="text/javascript" src="mathjax/MathJax.js"></script>


  <body>

		<div class="container">
		  <div class="header clearfix">
			<nav>
			  <ul class="nav nav-pills pull-right">
				<li role="presentation" class="active"><a href="#">Home</a></li>
				<li role="presentation"><a href="#">About</a></li>
				<li role="presentation"><a href="#">Contact</a></li>
			  </ul>
			</nav>
			<h3 class="text-muted">Project name</h3>
		  </div>

		  <div class="jumbotron">
			<h1><?php echo $Nombre_Noticia; ?></h1>
			<p class="lead">Cras justo odio, dapibus ac facilisis in, egestas eget quam. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
			<p><a class="btn btn-lg btn-success" href="#" role="button">Sign up today</a></p>
		  </div>

			<h1>MathJax Test Page</h1>

		<!-- Page Content -->
			<div class="row">

				<div class="col-sm-8 blog-main">

				  <div class="blog-post">
					<h2 class="blog-post-title">Sample blog post</h2>
					<p class="blog-post-meta">January 1, 2014 by <a href="#">Mark</a></p>

					<p> <?php echo $pdf; ?>
					</p>
					<ul class="list-unstyled">
						<li>Bootstrap v3.3.7</li>
						<li>jQuery v1.11.1</li>
					</ul>
			</div>
		</div>
		

		</div>
	</div>
</body>


    <!-- jQuery Version 1.11.1 -->
    <script src="src/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="src/js/bootstrap.min.js"></script>
</html>




