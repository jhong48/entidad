<?php 
	class Conexion{
	private $conexion;


	public function __construct(){
		$servidor = "localhost";
		$usuario = "root";
		$clave = "";
		$base = "entidad";
		
		$this->conexion = mysqli_connect($servidor,$usuario,$clave,$base );
		if(!$this->conexion)
			die("Conexion Fallida ". mysql_error);
	}
	
	public function __destruct(){
	
	}
	
	public function Listar($slq){
		$myArray = array();
		if ($result = mysqli_query($this->conexion, $slq)) {
			while($row = $result->fetch_array(MYSQLI_ASSOC)) {
				$myArray[] = $row;
			}
			$result->close();
			$this->cerrarconexion();
			return $myArray;
		}
	}
	
	public function cerrarfiltro($datos){
		mysqli_free_result($datos);
	}
	
	public function proximo($datos){
		$arreglo = mysqli_fetch_array($datos);
		return $arreglo;
	}
	
	public function buscar($slq, $busqueda){
		$resultRet = "0"; 
	if ($result = mysqli_query($this->conexion, $slq)) {
			while($row = mysqli_fetch_assoc($result)) {
					$resultRet =  $row[$busqueda];
			}
			$result->close();
			$this->cerrarconexion();
			return $resultRet;
		}
	} 
	
	public function imprimir($slq){
		$myArray = array();
		if ($result = mysqli_query($this->conexion, $slq)) {
			while($row = $result->fetch_array(MYSQL_ASSOC)) {
				$myArray[] = $row;
			}
			$result->close();
			$this->cerrarconexion();
			return $myArray;
		}
	}
	
	public function ejecutar($sql){
		$boolRet = "0";
		if(mysqli_query($this->conexion, $sql)){
			return "1";
		}else{
			return null;
		}
	}
	
	public function mostrar($slq){
		$myArray = array();
		$strResult = "";
		if ($result = mysqli_query($this->conexion, $slq)) {
			while($row = mysqli_fetch_assoc($result)) {
				$myArray[]= $row["Id_Persona"];
			}
				if(sizeof($myArray) >0)
				{
					$strResult = $myArray[0];
				}else {
						$strResult ="";
				}
		}
		
			$result->close();
			$this->cerrarconexion();
			return $strResult;
	}
	
	
		function utf8_string_array_encode(&$array){
				$func = function(&$value,&$key){
				if(is_string($value)){
					$value = utf8_encode($value);
				}		 
				if(is_string($key)){
					$key = utf8_encode($key);
				}
				if(is_array($value)){
				//	utf8_string_array_encode($value);
				}
    };
    array_walk($array,$func);
    return $array;
}
	
	
	public function cerrarconexion(){
	
	mysqli_close($this->conexion);
	
	}
	}


?>