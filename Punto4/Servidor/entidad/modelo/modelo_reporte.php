<?php	
	include_once('conexion/conexion.php');
	class modelo_reporte{
		private $datos;


		public function Listar_Reportes(){
			$datos = new conexion();
			$result = array();
			$slq = "Select rep.Id_Reporte, rep.Fecha_Registro, rep.Estado_Reporte, tip.Nombre_Reporte, usu.Id_Usuario, usu.Nombre_Usuario,
					noti.Id_Noticia, noti.Nombre_Noticia
					from tbl_reportes as rep
					join tbl_tipo_reporte as tip
					on rep.Id_Tipo_Reporte = tip.Id_Tipo_Reporte
					join tbl_usuarios as usu
					on rep.Id_Usuario = usu.Id_Usuario
					join tbl_noticias as noti
					on noti.Id_Noticia = rep.Id_Noticia
					where rep.Estado_Reporte=1";
			$result = $datos->Listar($slq);	
			return $result;
		}
			
			public function Buscar_Temas($strBusqueda){
			$datos = new conexion();
			$result = array();
			$slq = "
			SELECT Id_Tema, Nombre_Tema, Descripcion_Tema, Estado_Tema
			FROM tbl_tema where Nombre_Tema='$strBusqueda'";
			$result = $datos->Listar($slq);	
			//$res = $datos->utf8_string_array_encode($result);
	
			return $result;
			}
			/*
				SELECT noti.Id_Noticia, noti.Nombre_Noticia, noti.Imagen_Noticia, noti.Descripcion, noti.Tiempo_Publicacion, noti.url, noti.pdf,
					usu.Id_Usuario, usu.Nombre_Usuario, usu.contrasenia, usu.tiempo_ingreso, usu.Estado, usu.Tiempo_Autenticacion, usu.IP, usu.Puerto, usu.Imagen_Usuario,
					cat.Nombre_Categoria, cat.Id_Categoria
					FROM tbl_noticias noti JOIN tbl_usuario usu
					ON noti.id_usuario = usu.id_usuario
					JOIN tbl_categoria cat
					ON noti.id_categoria = cat.id_categoria;
		*/
	/*
			SELECT noti.Id_Noticia, noti.Nombre_Noticia, noti.Imagen_Noticia, noti.Descripcion, noti.Tiempo_Publicacion, noti.url, usu.Id_Usuario, 
			usu.Nombre_Usuario, usu.contrasenia, usu.tiempo_ingreso, usu.Estado, usu.Tiempo_Autenticacion, usu.IP, usu.Puerto, usu.Imagen_Usuario,
			gus.id_gustar,gus.Id_Noticia, gus.id_usuario, usuli.Nombre_Usuario		
					FROM tbl_noticias noti JOIN tbl_usuario usu
					ON noti.id_usuario = usu.id_usuario
					JOIN tbl_gustar gus
					ON gus.id_noticia = noti.id_noticia
					JOIN tbl_usuario usuli
					ON usuli.id_usuario = gus.id_usuario
					WHERE gus.Estado_Gustar = 1;
					
					
					
					SELECT noti.Id_Noticia, noti.Nombre_Noticia, noti.Imagen_Noticia, noti.Descripcion, noti.Tiempo_Publicacion, noti.url, noti.pdf,  usu.Id_Usuario, 
			usu.Nombre_Usuario, usu.contrasenia, usu.tiempo_ingreso, usu.Estado, usu.Tiempo_Autenticacion, usu.IP, usu.Puerto, usu.Imagen_Usuario,
			gus.id_gustar,gus.Id_Noticia, gus.id_usuario, usuli.Nombre_Usuario as 'UsuarioLike', gus.Estado_Gustar, gus.Fecha_Gustar as 'Fecha_Gustar', cat.Nombre_Categoria, cat.Id_Categoria 		
					FROM tbl_noticias noti JOIN tbl_usuario usu
					ON noti.id_usuario = usu.id_usuario
					JOIN tbl_gustar gus
					ON gus.id_noticia = noti.id_noticia
					JOIN tbl_usuario usuli
					ON usuli.id_usuario = gus.id_usuario
					JOIN tbl_categoria cat
					ON noti.id_categoria = cat.id_categoria;
					*/
		

		public function Registrar_Reporte($Id_Noticia, $Id_Usuario, $Id_Tipo_Reporte){
			$datos = new conexion();
			$slq ="insert into tbl_reportes (Id_Noticia, Id_Usuario, Id_Tipo_Reporte, Fecha_Registro) values ('$Id_Noticia', '$Id_Usuario', '$Id_Tipo_Reporte', now())";
			$reg = $datos->ejecutar($slq);
			if($reg == null){
				return "1";
			}else{
				return "0";
			}
		}
		
		public function Actualizar_Tema($Id_Tema, $Nombre_Tema, $Descripcion_Tema){
			$datos = new conexion();
			$slq ="update tbl_tema set Nombre_Tema = '$Nombre_Tema', Descripcion_tema = '$Descripcion_Tema' where Id_Tema= '$Id_Tema'";
			$reg = $datos->ejecutar($slq);
			if($reg == null){
				return "1";
			}else{
				return "0";
			}
		}
		
		public function Cargar_Tema($Id_Tema){
			$datos = new conexion();
			$result = array();
			$slq = "
				SELECT Id_Tema, Nombre_Tema, Descripcion_Tema, Estado_Tema
				FROM tbl_tema
				WHERE Id_Tema = '$Id_Tema'";
			$result = $datos->Listar($slq);	
			return $result;
		}
		
		public function Cambiar_Estado($Id_Tema, $Estado_Tema){
			$datos = new conexion();
			$slq ="update tbl_tema set Estado_Tema='$Estado_Tema' where Id_Tema= '$Id_Tema'";
			$reg = $datos->ejecutar($slq);
			if($reg == null){
				return "1";
			}else{
				return "0";
			}
		}
		
		public function Registrar_Noticia($Id_Usuario, $Nombre_Noticia, $Descripcion, $url,  $image_name, $base64_string){
			$boolRegGus = false;
			$datos = new conexion();
			if($base64_string != null &&  $image_name != null){
				$data = explode(',', $base64_string);
				$ifp = fopen($_SERVER['DOCUMENT_ROOT'] ."/retame/images_noticias/".$image_name, "wb");// use your folder path
				fwrite($ifp, base64_decode($data[1]));
				fclose($ifp);
				$sql = "INSERT INTO tbl_noticias  (Nombre_Noticia, Imagen_Noticia, Descripcion,Tiempo_Publicacion,url,Id_Usuario, Id_Categoria) VALUES ('$Nombre_Noticia', 'http://192.168.43.137/retame/images_noticias/$image_name','$Descripcion', NOW(), '$url', '$Id_Usuario', '1');";
				$boolRegGus = $datos->ejecutar($sql);
			}else{
				$boolRegGus = false;
			}
			return $boolRegGus;
	
		}
		
		public function Actualizar_IP($Id_Usuario, $Puerto){
			$boolRes = false;
			$datos = new conexion();
			$sql = "UPDATE  tbl_usuario SET Puerto = '$Puerto', IP = '".$_SERVER["REMOTE_ADDR"]."' WHERE Id_Usuario = '$Id_Usuario';";
			$boolRes = $datos->ejecutar($sql);
			return $boolRes;
		}
		
		public function Verficar_Tema($Nombre_Tema){
		
		}
	}

	
	
?>