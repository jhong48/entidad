<?php	
	include_once('conexion/conexion.php');
	class modelo_noticias{
		private $datos;


		public function Listar_Noticias($Usuario){
			$datos = new conexion();
			$result = array();
			$slq = "
				SELECT noti.Id_Noticia, noti.Nombre_Noticia, noti.Imagen_Noticia, noti.Descripcion_Noticia, noti.Fecha_Registro, noti.url, noti.Archivo_Noticia,
					usu.Id_Usuario, cat.Nombre_Categoria, cat.Id_Categoria, tem.Id_tema, tem.Nombre_Tema, mat.Id_Materia, mat.Nombre_Materia,
					per.Id_Persona
					FROM tbl_noticias noti JOIN tbl_usuarios usu
					ON noti.id_usuario = usu.id_usuario
                    join tbl_noticiasxcategorias nxc
                    on nxc.Id_NotxCat = noti.Id_NotxCat
					JOIN tbl_categorias cat
					ON nxc.id_categoria = cat.id_categoria
					JOIN tbl_personas per
					ON per.id_persona = usu.id_persona
					JOIN tbl_temas tem
					ON nxc.Id_Tema = tem.Id_Tema
                    join tbl_materias mat
                    on tem.Id_Materia = mat.Id_Materia
			
			
					;";
			$result = $datos->Listar($slq);	
			//$res = $datos->utf8_string_array_encode($result);
	
			return $result;
	/*

	
	SELECT noti.Id_Noticia, noti.Nombre_Noticia, noti.Imagen_Noticia, noti.Descripcion, noti.Tiempo_Publicacion, noti.url, noti.pdf,
					usu.Id_Usuario, usu.Nombre_Usuario, usu.contrasenia, usu.tiempo_ingreso, usu.Estado, usu.Tiempo_Autenticacion, usu.IP, usu.Puerto, usu.Imagen_Usuario,
					cat.Nombre_Categoria, cat.Id_Categoria, tem.Id_tema, tem.Nombre_Tema, tem.Descripcion_Tema,
					per.Id_Persona, per.Nombres, per.apellidos, per.movil
					FROM tbl_noticias noti JOIN tbl_usuario usu
					ON noti.id_usuario = usu.id_usuario
					JOIN tbl_categoria cat
					ON noti.id_categoria = cat.id_categoria
					JOIN tbl_persona per
					ON per.id_persona = usu.id_persona
					JOIN tbl_tema tem
					ON noti.Id_Tema = tem.Id_Tema
	
			SELECT noti.Id_Noticia, noti.Nombre_Noticia, noti.Imagen_Noticia, noti.Descripcion, noti.Tiempo_Publicacion, noti.url, noti.pdf,
					usu.Id_Usuario, usu.Nombre_Usuario, usu.contrasenia, usu.tiempo_ingreso, usu.Estado, usu.Tiempo_Autenticacion, usu.IP, usu.Puerto, usu.Imagen_Usuario,
					cat.Nombre_Categoria, cat.Id_Categoria, tem.Id_tema, tem.Nombre_Tema, tem.Descripcion_Tema,
					per.Id_Persona, per.Nombres, per.apellidos, per.movil, count(gus.Id_Noticia) as 'Total'
					FROM tbl_noticias noti JOIN tbl_usuario usu
					ON noti.id_usuario = usu.id_usuario
					JOIN tbl_categoria cat
					ON noti.id_categoria = cat.id_categoria
					JOIN tbl_persona per
					ON per.id_persona = usu.id_persona
					JOIN tbl_tema tem
					ON noti.Id_Tema = tem.Id_Tema
					left join tbl_gustar gus
                    on gus.Id_Noticia = noti.Id_Noticia
                    group by noti.Id_Noticia
                    
                    union
                    
SELECT noti.Id_Noticia, noti.Nombre_Noticia, noti.Imagen_Noticia, noti.Descripcion, noti.Tiempo_Publicacion, noti.url, noti.pdf,
					usu.Id_Usuario, usu.Nombre_Usuario, usu.contrasenia, usu.tiempo_ingreso, usu.Estado, usu.Tiempo_Autenticacion, usu.IP, usu.Puerto, usu.Imagen_Usuario,
					cat.Nombre_Categoria, cat.Id_Categoria, tem.Id_tema, tem.Nombre_Tema, tem.Descripcion_Tema,
					per.Id_Persona, per.Nombres, per.apellidos, per.movil, count(gus.Id_Noticia) as 'Total'
					FROM tbl_noticias noti JOIN tbl_usuario usu
					ON noti.id_usuario = usu.id_usuario
					JOIN tbl_categoria cat
					ON noti.id_categoria = cat.id_categoria
					JOIN tbl_persona per
					ON per.id_persona = usu.id_persona
					JOIN tbl_tema tem
					ON noti.Id_Tema = tem.Id_Tema
					Right join tbl_gustar gus
                    on gus.Id_Noticia = noti.Id_Noticia
                    group by noti.Id_Noticia
					*/
		}

		public function Listar_Ejemplos($Usuario){
			$datos = new conexion();
			$result = array();
			$slq = "
				SELECT noti.Id_Noticia, noti.Nombre_Noticia, noti.Imagen_Noticia, noti.Descripcion_Noticia, noti.Fecha_Registro, noti.url, noti.Archivo_Noticia,
					usu.Id_Usuario, cat.Nombre_Categoria, cat.Id_Categoria, tem.Id_tema, tem.Nombre_Tema, mat.Id_Materia, mat.Nombre_Materia,
					per.Id_Persona
					FROM tbl_noticias noti JOIN tbl_usuarios usu
					ON noti.id_usuario = usu.id_usuario
                    join tbl_noticiasxcategorias nxc
                    on nxc.Id_NotxCat = noti.Id_NotxCat
					JOIN tbl_categorias cat
					ON nxc.id_categoria = cat.id_categoria
					JOIN tbl_personas per
					ON per.id_persona = usu.id_persona
					JOIN tbl_temas tem
					ON nxc.Id_Tema = tem.Id_Tema
                    join tbl_materias mat
                    on tem.Id_Materia = mat.Id_Materia
                    where cat.Id_Categoria='1'";
			
					/*
							SELECT noti.Id_Noticia, noti.Nombre_Noticia, noti.Imagen_Noticia, noti.Descripcion, noti.Tiempo_Publicacion, noti.url, noti.pdf,
					usu.Id_Usuario, usu.Nombre_Usuario, usu.contrasenia, usu.tiempo_ingreso, usu.Estado, usu.Tiempo_Autenticacion, usu.IP, usu.Puerto, usu.Imagen_Usuario,
					cat.Nombre_Categoria, cat.Id_Categoria, tem.Id_tema, tem.Nombre_Tema, tem.Descripcion_Tema,
					per.Id_Persona, per.Nombres, per.apellidos, per.movil
					FROM tbl_noticias noti JOIN tbl_usuario usu
					ON noti.id_usuario = usu.id_usuario
					JOIN tbl_categoria cat
					ON noti.id_categoria = cat.id_categoria
					JOIN tbl_persona per
					ON per.id_persona = usu.id_persona
					JOIN tbl_tema tem
					ON noti.Id_Tema = tem.Id_Tema
                    where cat.Id_Categoria='1'";
					*/
			$result = $datos->Listar($slq);	
			//$res = $datos->utf8_string_array_encode($result);
	
			return $result;
		}
			public function Buscar_Noticia($Busqueda){
			$datos = new conexion();
			$result = array();
			$slq = "			
					SELECT noti.Id_Noticia, noti.Nombre_Noticia, noti.Imagen_Noticia, noti.Descripcion_Noticia, noti.Fecha_Registro, noti.url, noti.Archivo_Noticia,
					usu.Id_Usuario, cat.Nombre_Categoria, cat.Id_Categoria, tem.Id_tema, tem.Nombre_Tema, mat.Id_Materia, mat.Nombre_Materia,
					per.Id_Persona
					FROM tbl_noticias noti JOIN tbl_usuarios usu
					ON noti.id_usuario = usu.id_usuario
                    join tbl_noticiasxcategorias nxc
                    on nxc.Id_NotxCat = noti.Id_NotxCat
					JOIN tbl_categorias cat
					ON nxc.id_categoria = cat.id_categoria
					JOIN tbl_personas per
					ON per.id_persona = usu.id_persona
					JOIN tbl_temas tem
					ON nxc.Id_Tema = tem.Id_Tema
                    join tbl_materias mat
                    on tem.Id_Materia = mat.Id_Materia
					where noti.Nombre_Noticia LIKE  '%$Busqueda%'  or tem.Nombre_Tema LIKE '%%$Busqueda%';
					";
			$result = $datos->Listar($slq);	
			
			/*
				SELECT noti.Id_Noticia, noti.Nombre_Noticia, noti.Imagen_Noticia, noti.Descripcion, noti.Tiempo_Publicacion, noti.url, noti.pdf,
					usu.Id_Usuario, usu.Nombre_Usuario, usu.contrasenia, usu.tiempo_ingreso, usu.Estado, usu.Tiempo_Autenticacion, usu.IP, usu.Puerto, usu.Imagen_Usuario,
					cat.Nombre_Categoria, cat.Id_Categoria, tem.Id_tema, tem.Nombre_Tema, tem.Descripcion_Tema,
					per.Id_Persona, per.Nombres, per.apellidos, per.movil
					FROM tbl_noticias noti JOIN tbl_usuario usu
					ON noti.id_usuario = usu.id_usuario
					JOIN tbl_categoria cat
					ON noti.id_categoria = cat.id_categoria
					JOIN tbl_persona per
					ON per.id_persona = usu.id_persona
					JOIN tbl_tema tem
					ON noti.Id_Tema = tem.Id_Tema
                    where noti.Nombre_Noticia LIKE  '%$Busqueda%' or noti.Descripcion LIKE '%$Busqueda%' or tem.Nombre_Tema LIKE '%%$Busqueda%';
			*/
	
			return $result;
			}
		public function Registrar_Noticia($Id_Usuario, $Nombre_Noticia, $Descripcion, $url,  $image_name, $base64_string){
			$boolRegGus = false;
			$datos = new conexion();
			if($base64_string != null &&  $image_name != null){
				$data = explode(',', $base64_string);
				$ifp = fopen($_SERVER['DOCUMENT_ROOT'] ."/retame/images_noticias/".$image_name, "wb");
				fwrite($ifp, base64_decode($data[1]));
				fclose($ifp);
				$sql = "INSERT INTO tbl_noticias  (Nombre_Noticia, Imagen_Noticia, Descripcion,Tiempo_Publicacion,url,Id_Usuario, Id_Categoria) VALUES ('$Nombre_Noticia', 'http://192.168.43.137/retame/images_noticias/$image_name','$Descripcion', NOW(), '$url', '$Id_Usuario', '1');";
				$boolRegGus = $datos->ejecutar($sql);
			}else{
				$boolRegGus = false;
			}
			return $boolRegGus;
	
		}
		
		public function Actualizar_IP($Id_Usuario, $Puerto){
			$boolRes = false;
			$datos = new conexion();
			$sql = "UPDATE  tbl_usuario SET Puerto = '$Puerto', IP = '".$_SERVER["REMOTE_ADDR"]."' WHERE Id_Usuario = '$Id_Usuario';";
			$boolRes = $datos->ejecutar($sql);
			return $boolRes;
		}
		
		public function Cargar_Noticia($Id_Noticia){
			$result = array();
			$datos = new conexion();
			$slq = "SELECT Nombre_Noticia, Descripcion, pdf FROM tbl_noticias WHERE Id_Noticia = '$Id_Noticia'";
			$result = $datos->imprimir($slq);
		
			return $result;
		}
		
		public function almacenar_archivo($nombre, $cantlapiz, $cantcuaderno, $cantborrador, $subtotal, $direccion   ){
			$stringsalida = $nombre."\t".$cantlapiz." Lapices \t".$cantcuaderno." cuadernos\t"
                  .$cantborrador." borradores\t\$".$subtotal
                  ."\t". $direccion."\n";
			

				$fp = fopen("../pedidos/pedidos.txt", "a");
				flock($fp, 2);

				if (!$fp)
				  {
					echo "<p><strong> Su orden no ha podido ser procesada en este momento.  "
						 ."Por favor inténtelo de nuevo más tarde.</strong></p></body></html>";
					exit;
				  }
				  fwrite($fp, $stringsalida);
				  flock($fp, 3);
				  fclose($fp);
				  echo "<p>Su orden ha sido recibida y guardada.</p>";

		}
		
		public function abrir(){
			$fp=fopen('../pedidos/pedidos.txt','r'); //donde esta el archivo y como usarlo “r” solo lectura
			flock($fp, 2); // bloquear el archivo mientras un usuario esta usando el archivo
			if (!$fp) // condicional que en caso de que no haya archivo para leer muestra un mensaje
			{
			echo "<p><strong>No hay órdenes para mostrar, por favor revise más tarde. </strong></p></body></html>";
			exit;
			}
			while (!feof($fp)) // para leer un archivo hasta que llegue al final, usamos la función feof(), devuelve 
										//verdadero si esta al final
			{
			 $pedido=fgets($fp, 100); //fget función para leer, parámetros nombre de la vble que queremos leer y un valor
			//Con esta funcion el script lee de línea en línea
			Echo $pedido."<br>";
			}
			flock($fp,3);
			fclose($fp);

		}
	}

	
	
?>