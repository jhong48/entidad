<?php	
	include_once('conexion/conexion.php');
	class modelo_materias{
		private $datos;


		public function Listar_Materias($Opcion){
			$datos = new conexion();
			$result = array();
			if($Opcion == "1"){
				$slq = "
				SELECT Id_Materia, Nombre_Materia, Descripcion_Materia, Estado_Materia FROM tbl_materias;";
			}else{
				$slq = "
				SELECT Id_Materia, Nombre_Materia, Descripcion_Materia, Estado_Materia FROM tbl_materias where Estado_Materia = '1';";
			}			
			$result = $datos->Listar($slq);	
			//$res = $datos->utf8_string_array_encode($result);
	
			return $result;
		}

		public function Buscar_Materia($Busqueda){
			$datos = new conexion();
			$result = array();
			$slq = "			
					SELECT Id_Materia, Nombre_Materia, Descripcion_Materia, Estado_Materia FROM tbl_materias
					where Nombre_Materia LIKE  '%$Busqueda%'  or Descripcion_Materia LIKE '%$Busqueda%';
					";
			$result = $datos->Listar($slq);		
			return $result;
		}
			
		public function Registrar_Materia($Nombre_Materia, $Descripcion_Materia){
			$datos = new conexion();
			$slq ="insert into tbl_materias (Nombre_Materia, Descripcion_Materia, Estado_Materia) values ('$Nombre_Materia', '$Descripcion_Materia', '1')";
			$reg = $datos->ejecutar($slq);
			if($reg == null){
				return "1";
			}else{
				return "0";
			}
		}
		
		public function Actualizar_Materia($Id_Materia, $Nombre_Materia, $Descripcion_Materia){
			$datos = new conexion();
			$slq ="update tbl_materias set Nombre_Materia = '$Nombre_Materia', Descripcion_Materia = '$Descripcion_Materia' where Id_Materia= '$Id_Materia'";
			$reg = $datos->ejecutar($slq);
			if($reg == null){
				return "1";
			}else{
				return "0";
			}
		}
		
		public function Cambiar_Estado($Id_Materia, $Estado_Materia){
			$datos = new conexion();
			$slq ="update tbl_materias set Estado_Materia='$Estado_Materia' where Id_Materia= '$Id_Materia'";
			$reg = $datos->ejecutar($slq);
			if($reg == null){
				return "1";
			}else{
				return "0";
			}
		}
		
		public function Cargar_Materia($Id_Materia){
			$datos = new conexion();
			$result = array();
			$slq = "
				SELECT Id_Materia, Nombre_Materia, Descripcion_Materia, Estado_Materia FROM tbl_materias
				WHERE Id_Materia = '$Id_Materia'";
			$result = $datos->Listar($slq);	
			return $result;
		}
		
	}
?>