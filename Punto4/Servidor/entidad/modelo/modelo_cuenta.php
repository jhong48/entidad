<?php	
	include_once('conexion/conexion.php');
	class modelo_cuenta{
		private $datos;


		public function Registrar_Cuenta($Numero_Cuenta, $Id_Usuario){
			$datos = new conexion();
			$M ="";
			$U ="";
			$reg = "3";

		$M = $this->Verificar_Cuenta($Numero_Cuenta);
			if($M == "null"){
				$slq = "insert into tbl_cuentas (Numero_Cuenta) values ('$Numero_Cuenta');";
				$reg = $datos->ejecutar($slq);
				if($reg == null){
					return "3";
				}else{	
					$result = "";
					$result = $this->Ultimo_RegistroCuenta($Numero_Cuenta);
					if($result != "null"){
						$slq = "insert into tbl_usuarioxcuenta (Id_Usuario, Id_Cuenta) values ('$Id_Usuario', '$result');";
						//$slq = "insert into tbl_usuarios (Clave, Contrasenia, Id_Persona) values ('$Clave', '$Contrasenia', '$result');";
						$result = $datos->ejecutar($slq);
							if($result == null){
								return "4";
							}else{
								return "notnull";
							}
							
						}else{
							return "3";
						}
					}
			}else{
				return "1";
			}
		}
		
		public function Verificar_Cuenta($Numero_Cuenta){
			$datos = new conexion();
			$result="";
			$slq = "select Id_Cuenta from tbl_cuentas where Numero_Cuenta='$Numero_Cuenta'";
			$result = $datos->buscar($slq, "Id_Cuenta");
			if($result == "0"){
				return "null";
			}else{
				return $result;
			}
		}
		
		public function Ultimo_RegistroCuenta($Numero_Cuenta){
			$datos = new conexion();
			$result="";
			$slq = "select Id_Cuenta from tbl_cuentas where Numero_Cuenta='$Numero_Cuenta'";
			$result = $datos->buscar($slq, "Id_Cuenta");
			if($result == "0"){
				return "null";
			}else{
				return $result;
			}
		}
			
		public function Registrar_Movimiento($Numero_CuentaOr, $Numero_CuentaDest, $Cantidad, $Fecha_Transferencia,$Hora_Transferencia){
			$datos = new conexion();
			$M ="";
			$U =$Fecha_Transferencia.' '.$Hora_Transferencia;
			$reg = "3";
			$slq = "insert into tbl_transferencias (Cantidad_Transferencia, Fecha_Transferencia) values('$Cantidad', '$U')";
				$reg = $datos->ejecutar($slq);
				if($reg == null){
					return "3";
				}else{	
					$result = "";
					$slq = "insert into tbl_movimientos (Id_CuentaOrigen, Id_CuentaDestino, Id_Transferencia)
							values(
								(select Id_Cuenta from tbl_cuentas where Numero_Cuenta = '$Numero_CuentaOr'),
								(select Id_Cuenta from tbl_cuentas where Numero_Cuenta = '$Numero_CuentaDest'),
								(select Id_Transferencia from tbl_transferencias where Cantidad_Transferencia = '$Cantidad' and Fecha_Transferencia = '$U')
							)";
						$result = $datos->ejecutar($slq);
							if($result == null){
								return "4";
							}else{
								$slq = "select Id_Movimiento from tbl_movimientos mov
											join tbl_cuentas org on
											org.Id_Cuenta = mov.Id_CuentaOrigen
											join tbl_cuentas dest
											on dest.Id_Cuenta = mov.Id_CuentaDestino
											where org.Numero_Cuenta ='$Numero_CuentaOr' and dest.Numero_Cuenta = '$Numero_CuentaDest'";
								$result = $datos->Listar($slq);	
								return $result;
							}
							
					}
		}

		public function Listar_Movimientos($Id_Usuario){
			$datos = new conexion();
			$slq = "select  tran.Id_transferencia, mov.Id_CuentaOrigen, mov.Id_CuentaDestino, tran.Estado, tran.Cantidad_Transferencia, tran.Fecha_Transferencia
						from tbl_movimientos mov
						join tbl_transferencias tran
						on tran.Id_Transferencia = mov.Id_transferencia
						where  tran.Fecha_Transferencia > now() and  
						mov.Id_CuentaOrigen = (select cuentor.Id_Cuenta
						from tbl_movimientos movor
						join tbl_cuentas cuentor
						on movor.Id_CuentaOrigen = cuentor.Id_Cuenta
						join tbl_usuarioxcuenta usucueor
						on usucueor.Id_Cuenta = cuentor.Id_Cuenta
						join tbl_usuarios usuor
						on usuor.Id_Usuario = usucueor.Id_Usuario
						where usuor.Id_Usuario = '$Id_Usuario'
						order by movor.Id_Movimiento desc limit 1
						) or mov.Id_CuentaDestino = (select cuentdes.Id_Cuenta
						from tbl_movimientos movdes
						join tbl_cuentas cuentdes
						on movdes.Id_CuentaDestino = cuentdes.Id_Cuenta
						join tbl_usuarioxcuenta usucuedes
						on usucuedes.Id_Cuenta = cuentdes.Id_Cuenta
						join tbl_usuarios usudes
						on usudes.Id_Usuario = usucuedes.Id_Usuario
						where usudes.Id_Usuario = '$Id_Usuario' 
						order by movdes.Id_Movimiento desc limit 1 ) and tran.Estado = 'Activa'";					
			$result = $datos->Listar($slq);	
			return $result;
		}		
			
		public function Cambiar_EstadoTransferencia($Id_Transferencia, $Estado){
			$datos = new conexion();
	
			$slq = "update tbl_transferencias set Estado = '$Estado' where Id_Transferencia = '$Id_Transferencia'";
			$reg = $datos->ejecutar($slq);
			if($reg == null){
				return "3";
			}else{	
				$slq = "select Id_Transferencia from tbl_transferencias mov
						where Id_Transferencia = '$Id_Transferencia'";
				$result = $datos->Listar($slq);	
				return $result;
			}
		}
			
	}
?>